/* globals gauge*/
"use strict";
const path = require("path");
const {
  openBrowser,
  goto,
  write,
  closeBrowser,
  screenshot,
  click,
  text,
  into,
  textBox,
  $,
  waitFor,
} = require("taiko");
const assert = require("assert");
const headless = process.env.headless_chrome.toLowerCase() === "true";

beforeSuite(async () => {
  await openBrowser({
    headless: headless,
  });
});

afterSuite(async () => {
  await closeBrowser();
});

// Return a screenshot file name
gauge.customScreenshotWriter = async function () {
  const screenshotFilePath = path.join(
    process.env["gauge_screenshots_dir"],
    `screenshot-${process.hrtime.bigint()}.png`
  );

  await screenshot({
    path: screenshotFilePath,
  });
  return path.basename(screenshotFilePath);
};

step("Go to localhost", async () => {
  await goto("http://localhost:3000/");
});

step("Input area should be exist on page", async () => {
  assert.ok(await textBox("Add task").exists());
});
step("Should be a todo list", async () => {
  assert.ok(await $("#todos-list").exists());
});

step("Enter a todo <text>", async (text) => {
  await write(text, into($("#input-area")));
  await waitFor(3000);
});
step("Click <add> button", async (button) => {
  await click(button);
});
step("Todo item should be in the todo list <write>", async (write) => {
  assert.ok(await text(write).exists());
});
